<?php
/**
 * @name User
 * 		An object representing a user
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;

	class User extends Model
	{
		/** public properties */
		public $UserID = 0;
		public $Username = "";
		public $FirstName = "";
		public $LastName = "";
		public $Email = "";
		public $PhoneNumber = "";
		public $Updated = 0;
		public $Facebook = "";
		public $Twitter = "";
		public $Youtube = "";
		public $GooglePlus = "";
		public $LinkedIn = "";
		public $Instagram = "";
		public $SoundCloud = "";
		public $iTunes = "";
		public $NoiseTrade = "";
		public $GroupID = 0;
	}
}