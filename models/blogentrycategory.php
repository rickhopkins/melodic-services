<?php
/**
 * @name BlogEntryCategory
 * 		An object representing a blog entry category
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;
	
	class BlogEntryCategory extends Model
	{
		/** public properties */
		public $CategoryID = 0;
		public $EntryID = 0;
		public $Category = "";
	}
}
?>