<?php
/**
 * @name BlogEngine
 * 		An object representing a blog engine
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;
	
	class BlogEngine extends Model
	{
		/** public properties */
		public $EngineID = 0;
		public $EngineName = "";
		public $ImportFunction = "";
	}
}
?>