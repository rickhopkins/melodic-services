<?php
/**
 * @name BlogEntry
 * 		An object representing a blog entry
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;
	
	class BlogEntry extends Model
	{
		/** public properties */
		public $EntryID = 0;
		public $Title = "";
		public $Content = "";
		public $Published = 0;
		public $Updated = 0;
		public $OriginalUrl = "";
		public $Permalink = "";
		public $BlogID = 0;
	}
}
?>