<?php
/**
 * @name Blog
 * 		An object representing a blog
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;

	class Blog extends Model
	{
		/** public properties */
		public $BlogID = 0;
		public $BlogUrl = "";
		public $FeedUrl = "";
		public $Title = "";
		public $Description = "";
		public $Updated = 0;
		public $EngineID = 0;
		public $UserID = 0;
		public $TagFilter = "";
		public $IsActive = 0;
	}
}
?>