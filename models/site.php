<?php
/**
 * @name Site
 * 		An object representing the website
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;
	
	class Site extends Model
	{
		/** public properties */
		public $SiteID = 0;
		public $SiteName = "";
		public $Description = "";
		public $SiteUrl = "";
	}
}
?>