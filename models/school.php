<?php
/**
 * @name School
 * 		An object representing a school
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;

	class School extends Model
	{
		/** public properties */
		public $SchoolID = 0;
		public $SchoolName = "";
		public $State = "";
	}
}
?>