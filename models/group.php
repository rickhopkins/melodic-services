<?php
/**
 * @name Group
 * 		An object representing a user group
 * @author Rick Hopkins
 */

namespace Models
{
	use Melodic\DB\Model;

	class Group extends Model
	{
		/** public properties */
		public $GroupID = 0;
		public $GroupName = "";
	}
}