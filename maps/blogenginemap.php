<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;
	use Melodic\DB\Relationship;

	class BlogEngineMap extends Map
	{
		/**
		 * Initialize the BlogEngineMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\BlogEngine");
			
			/** setup the map */
			$this->setConstraints(array(
				"EngineName" => array(Constraint::MaxLength => 60),
				"ImportFunction" => array(Constraint::MaxLength => 100)
			))
			->setRelationships(array(
				new Relationship("\\Models\\Blog", "EngineID", Relationship::ONE_TO_MANY)
			));
		}
	}
}
?>