<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;
	use Melodic\DB\Relationship;

	class UserMap extends Map
	{
		/**
		 * Initialize the UserMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\User");
			
			/** setup the map */
			$this->setConstraints(array(
				"Username" => array(Constraint::MaxLength => 100),
				"FirstName" => array(Constraint::MaxLength => 60),
				"LastName" => array(Constraint::MaxLength => 60),
				"Email" => array(Constraint::MaxLength => 100),
				"PhoneNumber" => array(Constraint::MaxLength => 15),
				"Facebook" => array(Constraint::MaxLength => 255),
				"Twitter" => array(Constraint::MaxLength => 255),
				"Youtube" => array(Constraint::MaxLength => 255),
				"GooglePlus" => array(Constraint::MaxLength => 255),
				"LinkedIn" => array(Constraint::MaxLength => 255),
				"Instagram" => array(Constraint::MaxLength => 255),
				"SoundCloud" => array(Constraint::MaxLength => 255),
				"iTunes" => array(Constraint::MaxLength => 255),
				"NoiseTrade" => array(Constraint::MaxLength => 255)
			))
			->setRelationships(array(
				new Relationship("\\Models\\Group", "GroupID", Relationship::ONE_TO_ONE)
			));
		}
	}
}
?>