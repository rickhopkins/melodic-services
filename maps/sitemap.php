<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;

	class SiteMap extends Map
	{
		/**
		 * Initialize the SiteMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\Site");
			
			/** setup the map */
			$this->setConstraints(array(
				"SiteName" => array(Constraint::MaxLength => 255),
				"Description" => array(Constraint::MaxLength => 255),
				"SiteUrl" => array(Constraint::MaxLength => 255)
			));
		}
	}
}
?>