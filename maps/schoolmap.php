<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;
	use Melodic\DB\Relationship;

	class SchoolMap extends Map
	{
		/**
		 * Initialize the SchoolMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\School");
		}
	}
}
?>