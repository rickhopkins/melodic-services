<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;
	use Melodic\DB\Relationship;

	class BlogEntryMap extends Map
	{
		/**
		 * Initialize the BlogEntryMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\BlogEntry");
			
			/** setup the map */
			$this->setConstraints(array(
				"Title" => array(Constraint::MaxLength => 255),
				"OriginalUrl" => array(Constraint::MaxLength => 255),
				"Permalink" => array(Constraint::MaxLength => 255)
			))
			->setRelationships(array(
				new Relationship("\\Models\\Blog", "BlogID", Relationship::ONE_TO_ONE),
				new Relationship("\\Models\\BlogEntryCategory", "EntryID", Relationship::ONE_TO_MANY)
			));
		}
	}
}
?>