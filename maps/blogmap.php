<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;
	use Melodic\DB\Relationship;

	class BlogMap extends Map
	{
		/**
		 * Initialize the BlogMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\Blog");
			
			/** setup the map */
			$this->setConstraints(array(
				"BlogUrl" => array(Constraint::MaxLength => 255),
				"FeedUrl" => array(Constraint::MaxLength => 255),
				"Title" => array(Constraint::MaxLength => 255),
				"Description" => array(Constraint::MaxLength => 255),
				"TagFilter" => array(Constraint::MaxLength => 255),
			))
			->setRelationships(array(
				new Relationship("\\Models\\BlogEngine", "EngineID", Relationship::ONE_TO_ONE),
				new Relationship("\\Models\\BlogEntry", "BlogID", Relationship::ONE_TO_MANY)
			));
		}
	}
}
?>