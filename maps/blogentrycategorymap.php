<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;
	use Melodic\DB\Relationship;

	class BlogEntryCategoryMap extends Map
	{
		/**
		 * Initialize the BlogEntryCategoryMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\BlogEntryCategory");
			
			/** setup the map */
			$this->setConstraints(array(
				"Category" => array(Constraint::MaxLength => 255)
			))
			->setRelationships(array(
				new Relationship("\\Models\\BlogEntry", "EntryID", Relationship::ONE_TO_ONE)
			));
		}
	}
}
?>