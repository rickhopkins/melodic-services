<?php
namespace Maps
{
	use Melodic\DB\Map;
	use Melodic\DB\Constraint;
	use Melodic\DB\Relationship;

	class GroupMap extends Map
	{
		/**
		 * Initialize the GroupMap
		 */
		public function __construct()
		{
			/** initialize the parent */
			parent::__construct("\\Models\\Group");
			
			/** setup the map */
			$this->setConstraints(array(
				"GroupName" => array(Constraint::MaxLength => 30)
			))
			->setRelationships(array(
				new Relationship("\\Models\\User", "GroupID", Relationship::ONE_TO_MANY)
			));
		}
	}
}
?>