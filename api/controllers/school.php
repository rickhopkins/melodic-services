<?php
/**
 * @name \API\Controllers\School
 * 		An API for School data
 * @author Rick Hopkins
 * @package Melodic
 */

namespace API\Controllers
{
	use API\interfaces\iMelodicUnitOfWork;
	use Melodic\DB\Model;
	use Melodic\MVC\ApiController;
	use Melodic\MVC\Route;

	class School extends ApiController
	{
		/** public properties */
		public $unitOfWork;

		/**
		 * Initialize the School controller
		 * @param Route $route
		 * @param iMelodicUnitOfWork $unitOfWork
		 */
		public function __construct(Route $route, iMelodicUnitOfWork $unitOfWork)
		{
			/** initialize the parent */
			parent::__construct($route);

			/** set the model */
			$this->model = "\\Models\\School";

			/** set the unit of work */
			$this->unitOfWork = $unitOfWork;

			/** return Blog */
			return $this;
		}

		/**
		 * Get a specific record
		 * @param $id
		 * @return Model
		 */
		public function Get($id)
		{
			return $this->unitOfWork->SchoolRepository->get($id);
		}

		/**
		 * Get all records of type, or execute query
		 * @param string $query
		 * @return array
		 */
		public function GetAll($query = "")
		{
			return $this->unitOfWork->SchoolRepository->getAll($query);
		}

		/**
		 * Create a new record
		 * @param Model $model
		 * @return Model
		 */
		public function Post(Model $model)
		{
			return $this->unitOfWork->SchoolRepository->save($model);
		}

		/**
		 * Update a record
		 * @param Model $model
		 * @return Model
		 */
		public function Put($id, Model $model)
		{
			return $this->unitOfWork->SchoolRepository->save($model);
		}

		/**
		 * Delete a record
		 * @param $id
		 * @return void
		 */
		public function Delete($id)
		{
			return $this->unitOfWork->SchoolRepository->delete($id);
		}
	}
}
?>