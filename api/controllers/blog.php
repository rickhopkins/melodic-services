<?php
/**
 * @name \API\Controllers\Blog
 * 		An API for Blog data
 * @author Rick Hopkins
 * @package Melodic
 */

namespace API\Controllers
{
	use API\MelodicUnitOfWork;
	use Melodic\DB\Model;
	use Melodic\MVC\ApiController;
	use Melodic\MVC\Route;

	class Blog extends ApiController
	{
		/** public properties */
		public $unitOfWork;

		/**
		 * Initialize the Blog controller
		 * @param Route $route
		 */
		public function __construct(Route $route)
		{
			/** initialize the parent */
			parent::__construct($route);

			/** set the model */
			$this->model = "\\Models\\Blog";

			/** set the unit of work */
			$this->unitOfWork = new MelodicUnitOfWork();

			/** return Blog */
			return $this;
		}

		/**
		 * Get a specific record
		 * @param $id
		 * @return Model
		 */
		public function Get($id)
		{
			return $this->unitOfWork->BlogRepository->get($id);
		}

		/**
		 * Get all records of type, or execute query
		 * @param string $query
		 * @return array
		 */
		public function GetAll($query = "")
		{
			return $this->unitOfWork->BlogRepository->getAll($query);
		}

		/**
		 * Create a new record
		 * @param Model $model
		 * @return Model
		 */
		public function Post(Model $model)
		{
			return $this->unitOfWork->BlogRepository->save($model);
		}

		/**
		 * Update a record
		 * @param Model $model
		 * @return Model
		 */
		public function Put($id, Model $model)
		{
			return $this->unitOfWork->BlogRepository->save($model);
		}

		/**
		 * Delete a record
		 * @param $id
		 * @return void
		 */
		public function Delete($id)
		{
			return $this->unitOfWork->BlogRepository->delete($id);
		}
	}
}
?>