<?php
/**
 * @name \API\MelodicUnitOfWork
 * 		The unit of work for the melodic services
 * @author Rick Hopkins
 * @package Melodic
 */

namespace API
{
	use API\interfaces\iMelodicUnitOfWork;
	use Melodic\Config;
	use Melodic\DB\Context;
	use Melodic\DB\Repository;
	use Melodic\DB\UnitOfWork;

	class MelodicUnitOfWork extends UnitOfWork implements iMelodicUnitOfWork
	{
		/**
		 * Initialize the Melodic Services unit of work
		 */
		public function __construct()
		{
			/** get the db config */
			$context = new Context(Config::get("database")["MelodicContext"]);

			/** add the repositories */
			$this->addRepository("SchoolRepository", new Repository($context, new \Maps\SchoolMap()));

			// $this->addRepository("BlogRepository", new Repository($context, new \Maps\BlogMap()));
			// $this->addRepository("BlogEngineRepository", new Repository($context, new \Maps\BlogEngineMap()));
			// $this->addRepository("BlogEntryCategoryRepository", new Repository($context, new \Maps\BlogEntryCategoryMap()));
			// $this->addRepository("BlogEntryRepository", new Repository($context, new \Maps\BlogEntryMap()));
			// $this->addRepository("GroupRepository", new Repository($context, new \Maps\GroupMap()));
			// $this->addRepository("SiteRepository", new Repository($context, new \Maps\SiteMap()));
			// $this->addRepository("UserRepository", new Repository($context, new \Maps\UserMap()));
		}
	}
}
?>