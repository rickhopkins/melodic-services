<?php
/**
 * @name \API\iMelodicUnitOfWork
 * 		An interface defining the Melodic Unit Of Work
 * @author Rick Hopkins
 * @package Melodic
 */

namespace API\interfaces
{
	interface iMelodicUnitOfWork {}
}
?>