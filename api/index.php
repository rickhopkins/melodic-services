<?php
/** get the required file */
require_once "Melodic/Core.php";

/** create RouteMap and execute */
$route = new Melodic\MVC\Route();
$route->render();
?>